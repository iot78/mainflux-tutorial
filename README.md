# Mainflux Platform Tutorial

## :anchor: Prerequisites
The following are needed to run Mainflux:
* [Docker](https://docs.docker.com/get-docker/) (version 20.10) 
* [Docker compose](https://docs.docker.com/compose/install/) (version 1.29)

Note: Install docker and docker-compose using the guide from the official docker page.

The following are needed to run this tutorial:
* git
* go

## 🚀 Quick start

**Get the code** 

For instalattion access https://github.com/mainflux/mainflux
    or run command:
```shell
git clone https://github.com/mainflux/mainflux
```
Once the prerequisites are installed, execute the following commands from the project's root:

```shell
cd mainflux
docker-compose -f docker/docker-compose.yml up
```
or

This will bring up the Mainflux docker services and interconnect them. This command can also be executed using the project's included Makefile:

```shell
cd mainflux
make run
```

## :dart: Install Mainflux CLI

The quickest way to start using Mainflux is via the CLI. The latest version can be downloaded from the [official releases page](https://github.com/mainflux/mainflux/releases).

It can also be built and used from the project's root directory:

```shell
make cli
```

Note: For improve the usability mainflux create the environment variable

```shell
export PATH=$PATH:$(pwd)/build
```

Additional details on using the CLI can be found in the [CLI documentation](https://docs.mainflux.io/cli/).

**Usage** 

Get the version of Mainflux services:

```shell
mainflux-cli version
```

**Users Management**

Create User
```shell
mainflux-cli users create <user_email> <user_password>
```

Note: The combination of letters and numbers in the password is mandatory. Eg: `Nerds123`

Login User
```shell
mainflux-cli users token <user_email> <user_password>
```

After creating the user, note that a token will be generated, copy this token and go to the next step

Create Environment Variable
```shell
export USERTOKEN=<Generated key>
```

## :dart: Install Mainflux ui

For a quick setup, pre-built images from Docker Hub can be used.

First, make sure that docker and docker-compose are installed. Also, stop existing Mainflux containers if any.

Then, use the following instructions:

```shell
git clone https://github.com/mainflux/ui.git
cd ui
make run
```
UI should be now up and running at http://localhost/.

More configuration (port numbers, etc.) can be done by editing the .env file before make run.

**Usage ui** 

A developer build from the source can be achieved using the following command:

```shell
make ui
```

Then, to start the Mainflux UI as well as other Mainflux services:

```shell
make run
```

**Preview** 

![Image 1](src/images/mainflux-principal.png)

![Image 2](src/images/mainflux-thing.png)

![Image 3](src/images/mainflux-channel.png)

## :writing_hand: Hands on

**Create Thing** 

```shell
mainflux-cli things create '{"name":"TestThing"}' <user_auth_token>
```

```shell
mainflux-cli things create '{"name":"TestApp"}' <user_auth_token>
```

**View Thing**

```shell
mainflux-cli things get all <user_auth_token>
```

**Create Channel**

```shell
mainflux-cli channels create '{"name":"TestChannel"}' <user_auth_token>
```

**Connect Thing to Channel**

```shell
mainflux-cli things connect <id_TestThing> <id_TestChannel> <user_auth_token>
mainflux-cli things connect <id_TestApp> <id_TestChannel> <user_auth_token>
```

**View Channels**

```shell
mainflux-cli things get all <user_auth_token>
```

**Send a message over HTTP**

```shell
mainflux-cli messages send <id_TestChannel> '[{"bn":"Dev1","n":"temp","v":20}, {"n":"hum","v":40}, {"bn":"Dev2", "n":"temp","v":20}, {"n":"hum","v":40}]' <thing_auth_token>
```

**Publisher MQTT**

To send and receive messages over MQTT you could use Mosquitto tools, or Paho if you want to use MQTT over WebSocket.

To publish message over channel, thing should call following command:

```shell
mosquitto_pub -u <id_TestThing> -P <key_TestThing> -t channels/<id_TestChannel>/messages -h localhost -m '[{"bn":"some-base-name:","bt":1.276020076001e+09, "bu":"A","bver":5, "n":"voltage","u":"V","v":120.1}, {"n":"current","t":-5,"v":1.2}, {"n":"current","t":-4,"v":1.3}]'
```

**Subscribe MQTT**

To subscribe to channel, thing should call following command:

```shell
mosquitto_sub -u <id_TestApp> -P <key_TestApp> -t channels/<id_TestChannel>/messages -h localhost
```


## Authors

Edgard da Cunha Pontes\
Everson Scherrer Borges


## References:

[Oficial Page](https://github.com/mainflux/mainflux)
